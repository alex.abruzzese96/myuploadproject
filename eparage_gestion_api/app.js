const express = require('express');
const app = express();
const fs = require("fs")
var readdirp = require('readdirp');
var path = require('path');

var settings = {
    root: '.',
    entryType: 'all'
};
var allFilePaths = [];

app.get('/', (req, res) => {
    readdirp(settings, (fileInfo) => {
        allFilePaths.push({path: fileInfo.fullPath});
    },  
    (err, res) => {
        if(err) throw err;
    });
    console.log(allFilePaths);
    res.json(allFilePaths);
});

app.get('/getAllFiles', (req, res) => {
    res.send(getAllFiles('.'));
});

const getAllFiles = function(dirPath, arrayOfFiles) {
    files = fs.readdirSync(dirPath)
    
    arrayOfFiles = arrayOfFiles || []
    
    files.forEach(function(file) {
        if (fs.statSync(dirPath + "/" + file).isDirectory()) {
        arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles)
        } else {
        arrayOfFiles.push({path: path.join(__dirname, dirPath, "/", file)})
        }
    })

    return arrayOfFiles
};

app.listen(3000, () => {
    console.info('Server started on port 3000');
});