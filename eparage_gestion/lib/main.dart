import 'package:eparage_gestion/src/ui/drawer.dart';
import 'package:eparage_gestion/src/ui/getterDashboard.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({ Key? key, required this.title }) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => PageInformation();
}

class PageInformation extends State<MyHomePage> {
  String page = "";

  void changeData(data) {
    setState(() {
      page = data;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Gestionnaire de fichiers"),
      ),
      body: const MenuGetterPage(),
      // drawer: const MenuDashboardPage(),
    );
  }

  Widget setView(info) {
    switch (info) {
      case "getDocuments":
      print("ok");
        return GestureDetector(
          child: Container(
            color: Colors.blue,
            child: const Text("ok google !"),
          ),
        );
      default:
        return GestureDetector(
          onTap: () => { changeData("getDocuments" )},
          child: Container(
            color: Colors.red,
            child: const Text('change'),
          ),
        );
    }
  }
}
