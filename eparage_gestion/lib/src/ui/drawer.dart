import 'package:flutter/material.dart';

class MenuDashboardPage extends StatefulWidget {
  const MenuDashboardPage({ Key? key }) : super(key: key);

  @override
  _MenuDashboardPageState createState() => _MenuDashboardPageState();
}

class _MenuDashboardPageState extends State<MenuDashboardPage> {
  final padding = const EdgeInsets.symmetric(horizontal: 20);
  late double screenWidth, screenHeight;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    screenHeight = size.height;
    screenWidth = size.width;
    
    return Drawer(
      child: Material(
        color: Colors.blue,
        child: ListView(
          padding: padding,
          children: <Widget>[
            SizedBox(height: screenHeight / 10),
            ListTile(
              leading: const Icon(Icons.send, color: Colors.white),
              title: const Text("Envoyer mes documents", style: TextStyle(color: Colors.white)),
              hoverColor: Colors.white70,
              onTap: () => { print('ok') },
            ),
            ListTile(
              leading: const Icon(Icons.get_app, color: Colors.white),
              title: const Text("Récupérer mes documents", style: TextStyle(color: Colors.white)),
              hoverColor: Colors.white70,
              onTap: () => { print('ok') },
            ),
            ListTile(
              leading: const Icon(Icons.feedback, color: Colors.white),
              title: const Text("Contacter le développeur", style: TextStyle(color: Colors.white)),
              hoverColor: Colors.white70,
              onTap: () => { print('ok') },
            ),
            ListTile(
              leading: const Icon(Icons.update_outlined, color: Colors.white),
              title: const Text("Version du logiciel", style: TextStyle(color: Colors.white)),
              hoverColor: Colors.white70,
              onTap: () => { print('ok') },
            ),
            SizedBox(height: screenHeight / 1.8),
            const Align(
              alignment: Alignment.bottomCenter,
              child: Text("Développé avec ♥ par @Zinoka", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white)),
            ),
          ],
        ),
      ),
    );
  }
}

