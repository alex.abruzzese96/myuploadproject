import 'dart:convert';
import 'package:file_picker/file_picker.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart';

import 'drawer.dart';

class MenuGetterPage extends StatefulWidget {
  const MenuGetterPage({Key? key}) : super(key: key);

  @override
  _MenuGetterState createState() => _MenuGetterState();
}

class _MenuGetterState extends State<MenuGetterPage> {
  int fuckingTest = 0;

  final padding = const EdgeInsets.symmetric(horizontal: 20);
  String filesFromAPI = "";
  List<String> previousPathList = [];
  List<String> nextPath = [];
  String splittedArray = "";
  bool isEmpty = true;
  bool isInit = false;
  int toto = 0;
  int position = 0;
  late double screenWidth, screenHeight;
  List<String?> filesPath = [];
  List<String?> filesName = [];
  List<String> destinationArray = [];
  List<String> filesFromAPIArray = [];
  String pathtodisplay = "";

  @override
  void initState() {
    super.initState();
    getAllFiles();
  }

  Future<void> getAllFiles() async {
    try {
      Response res = await get(Uri.parse("http://localhost:3000/getAllFiles")); // 90.61.137.156:3000;

      for (var filePath in json.decode(res.body)) {
        setState(() {
          filesFromAPIArray.add(filePath['path']);
          nextPath.add(filePath['path']);
        });
      }
      isInit = true;
    } catch (err) {
      print(err);
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    screenHeight = size.height;
    screenWidth = size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      body: Scaffold(
        body: isInit ? SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Table(
              border: TableBorder.all(),
              columnWidths: const <int, TableColumnWidth>{
                0: IntrinsicColumnWidth(),
                1: FlexColumnWidth(),
                2: FixedColumnWidth(64),
              },
              defaultVerticalAlignment: TableCellVerticalAlignment.middle,
              children: setPath(filesName)
            ),
            ElevatedButton (
              child: const Text("Choisir le(s) fichier(s)"),
              
              onPressed: () async {
                await chooseFilePath();
              },
            )]
        )
      ) : const Text('Chargement en cours'))
    );
  }

  List<TableRow> setPath(pathArray) {
    List<TableRow> list = <TableRow>[];
    
    for(var path in pathArray) {
      list.add(TableRow(
        children: <Widget>[
          TableCell(
            verticalAlignment: TableCellVerticalAlignment.top,
            child: Text(path)
          ),
          TableCell(
            verticalAlignment: TableCellVerticalAlignment.top,
            child: GestureDetector(
              onTap: () => { 
                showDialog(context: context, builder: (BuildContext context) { 
                  return AlertDialog(
                    content: SingleChildScrollView(
                      child: ListBody(
                        children: [
                          Row (
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Table(
                                border: TableBorder.all(),
                                columnWidths: const <int, TableColumnWidth>{
                                  0: IntrinsicColumnWidth(),
                                  1: FlexColumnWidth(),
                                  2: FixedColumnWidth(64),
                                },
                                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                                children: [
                                  TableRow(
                                    children: [
                                      TableCell(
                                        verticalAlignment: TableCellVerticalAlignment.top,
                                        child: GestureDetector(
                                          onTap: () => { /*print(previousPathList[position])*/ },
                                          child: const Text("Non !!"),
                                        )
                                      )
                                    ]
                                  )
                                ]
                              ),
                              Table(
                                border: TableBorder.all(),
                                columnWidths: const <int, TableColumnWidth>{
                                  0: IntrinsicColumnWidth(),
                                  1: FlexColumnWidth(),
                                  2: FixedColumnWidth(64),
                                },
                                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                                children: setChoosePath(nextPath)
                              ),]
                            )],
                        ),
                      ), 
                      actions: [
                        ElevatedButton(onPressed: () => { setDestinationFolders(destinationArray, context) }, child: const Text("ok"))
                      ],
                  );
                })},
              child: const Text("Choisir la destination"),
            )
          ),
          const TableCell(
            verticalAlignment: TableCellVerticalAlignment.top,
            child: Text('peut etre')
          )]
      ));
    }
    return list;
  }


  List<TableRow> setChoosePath(nextPath) {
    List<TableRow> list = <TableRow>[];

    for(var path in nextPath) {
      list.add(TableRow(
        children: <Widget>[
          // ça bloque ici
          ElevatedButton (
            onPressed: () { setState(() {
              fuckingTest++;
            }); },
            child: Text('$fuckingTest'),
          ),
          TableCell(
            verticalAlignment: TableCellVerticalAlignment.top,
            child: GestureDetector(
              onTap: () => { setState(() { previousPathList.add(path); }), splitter(path, toto) },
              child: isEmpty ? Text(path) : Text(pathtodisplay),
            )
          ),
        ]
      ));
    } 

    return list;
  }

  splitter(path, position) {
    var test = path.split('\\');
    print(test[position]);
    try {
      setState(() {
        pathtodisplay = test[toto + 1];
        isEmpty = false;
        toto++;
      });
    } catch (err) {
      print(err);
    }
  }


  Future<FilePickerResult?> chooseFilePath() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(allowMultiple: true);
    if (result != null) {
      for (var file in result.files) {
        setState(() {
          filesPath.add(file.path);
          filesName.add(file.name);
        });
      }
    }
    return result;
  }

  setDestinationFolders(destinationArray, context) {
    print(destinationArray);
    Navigator.of(context).pop();
  }

  /*Future<String> uploadImage(filename) async {
    var request = http.MultipartRequest('POST', Uri.parse("http://localhost:3000"));

    request.files.add(await http.MultipartFile.fromPath('picture', filename));
    var res = await request.send();

    return res.reasonPhrase;
  }*/
}
